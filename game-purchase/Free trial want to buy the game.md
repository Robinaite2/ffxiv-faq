# What do I buy if I am on the free trial

## Short Answer

### Windows/Mac

**IMPORTANT!** The moment you buy the full game either on Steam or on the Square Enix Store, you will need to buy all future expansion on that respective store. No mix and match between steam and Square Enix versions.

If you are on the **Square Enix** Free trial:

- Square Enix Store Complete Edition
- Steam Complete edition (Make sure to uninstall the free trial before installing it again from steam.)

If you are on **Steam** Free trial:

- Steam Complete Edition

### Playstation

If you are on the Playstation free trial, buy the Complete Edition from the PSN Store.

## Long Answer

The Complete edition is a bundle which includes:

- The Starter Edition
  - ARR & HW Content
  - 30 days of "free" Sub time
- The latest expansion (Shadowbringers)
  - SB & ShB Content

(When Endwalker releases the latest expansion will be Endwalker, and include ShB, and SB)

## Extra Info

You can fine more info on the differences between the Steam and Square Enix Store versions here:  [What are the differences between steam and SE Store versions](What%20are%20the%20differences%20between%20steam%20and%20SE%20Store%20versions.md)
