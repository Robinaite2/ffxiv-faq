# What are the differences between the Steam and SE Store versions?

## Short Answer

If you are on the steam version you will be able to:

- Use your steam wallet to pay for the game, subscription and Online Store.

## Longer Answer (optional)

The Steam and SE Store versions are the same game, only differing in payment options, and regions availability. The versions are cross-play with each other

Steam allows the user to pay for their subscription, or cash shop items using the steam wallet.

It's also made avaialbe in more regions than the SE Store version.

Make sure that if you buy into one or the other, you will always need to buy any expansion in the future on the respective store.
