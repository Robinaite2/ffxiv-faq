# What are the free trial restrictions?

## Short Answer

The free trial is mostly restricted in social aspects and others such as:

- You can **not** send tells/whispers
- You can **not** add friends. (You can be added by someone who has the full game)
- You can **not** join a Free Company (FFXIV Player Guilds)
- You can **not** invite other players to a party (You can be invited to a party by someone who has the full game)
- You can **not** use the Market Board
- You can **not** do PvP content
- Gill cap is 300k.

And various others

What you can do however:

- Play all the content that is included in the base game ARR (A Realm Reborn) and the *critically acclaimed* expansion heavensward.
- Level all base game and Heavensward expansion jobs to level 60.
- Enjoy some great music and amazing story!
- ***No time limit on the free trial***

For a quicker rundown you can watch Zeplas video here: ![FFXIV FREE TRIAL Restrictions - What You CAN and CAN'T Do!](https://youtu.be/wjMtI6Ied0M)

## Longer Answer

You can find the official restrictions list here: [Square Enix Support Center](https://support.na.square-enix.com/rule.php?id=5382&tag=freetrial)
