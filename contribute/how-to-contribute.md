# How to contribute to the FFXIV FAQ

```md

---

Tags:
- tag 1
---

# Main Question

## Short Answer

*Input Short Answer*

## Longer Answer (optional)

*Input Longer answer*

## Extra Info (optional)

*Extra info*
```

The questions will be put in one of the following folders, take into mind that this is puerly for my own organization.

- Game Purchase/subscriptions - Contains questions ranging from how to buy the game, free trial restrictions and other content.
- Leveling - Contains everything related to the leveling experience and issues with progressions during leveling
- Gameplay
- Jobs (both battle jobs and crafters/Gatheres)
- Others

Some questions can be in multiple categories, but as indicated above, the folders is purely for self organization. There will be a need to create a better system to go through all these files.
