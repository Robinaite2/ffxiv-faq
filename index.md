---
title: FFXIV Sprout Friendly FAQ
description: Here you can find a lot of questions answered about the game, ranging from How to buy the game, to when do I get a mount!
---
# FFXIV Sprout FAQ

Use the sidebar or search to look for your questions.

Below you can find the most often asked questions:

* [I am Level 30 why no job advancements](battle-jobs/I am level 30 why no job advancement quest.md)
  