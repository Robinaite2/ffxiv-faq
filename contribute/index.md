# How to contribute or report issues

## How to report an issue

When you are reporting an issue please use the following template:

```
Page URL:
Issue:
Steps to reproduce:
```

You can report an issue through the following methods:

1. Open an issue on the gitlab repository found here: [https://gitlab.com/Robinaite/ffxiv-faq/-/issues](https://gitlab.com/Robinaite/ffxiv-faq/-/issues). 
2. Report it to Robinaite on twitter (either tweet at me, or DM). [Robinaite Twitter](https://twitter.com/robinaite)
3. Fix the issue yourself! Read below on How to contribute to find out more!


## How to Contribute

There are two ways to contribute to the project:

1. Updating pages and adding more information
2. Adding new questions and answers or guides.

### I want to fix an issue on a page or add more information to an existing page

Pre-requisites:
- Having a [Gitlab](https://gitlab.com/) account.

Follow the following steps:

1. Go to the page with the issue.
2. Click the edit button next to the title
3. It will redirect you to gitlab
4. Click on the edit button
    1. If its your first time editing the project, it will indicate "You can’t edit files directly in this project. Fork this project and submit a merge request with your changes." click Fork.
5. Edit the texts with the issue. Please make sure you follow the guidelines found here. (TODO ADD GUIDELINES) 
6. When finished, scroll down and update the Commit Message with a small description of what you changed.
7. Click Commit changes
8. It will load the page to make a new Merge Request.
    1. Update title and description if you want (not necessary)
9. Scroll down and click "Create Merge Request"
10. Now you wait for someone with access to the repository to review your changes and merge (update) them in the project. This can take up to 24 hours.

*If you are knowledgeable with git and version control systems, feel free to use git to create a merge request, you do not need to follow the steps above.*

### I want to add a new question or page to the website





